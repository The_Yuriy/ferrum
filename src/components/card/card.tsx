import React from 'react';

import styles from './card.module.scss';

export function Card() {
    return (
        <div className={styles.cardContainer}>
            <a href="#" className={styles.itemUrl}>
                <img src="https://www.chinasteelgrating.com/images/welded-steel-grating-smooth.jpg" alt="item_image"/>
            </a>
            <a href="#" className={styles.itemTitle}>Title</a>
            <div className={styles.actionsContainer}>
                <span>20 гривень</span>
                <button>Customize</button>
            </div>
        </div>
    );
}
