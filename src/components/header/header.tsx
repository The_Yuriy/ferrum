import React from 'react';

import {LogoIcon} from "../icons";

import styles from  './header.module.scss';

export function Header () {
    return (
        <header className={styles.header}>
            <LogoIcon className={styles.logo} />
            <div className={styles.buttons_container}>
                <button>Sign in</button>
            </div>
        </header>
    );
}