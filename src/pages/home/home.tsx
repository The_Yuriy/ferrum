import React from 'react';

import { Card } from "../../components/card/card";
import { Header } from "../../components/header/header";

import styles from './home.module.scss'

export function HomePage()
{
    return (
        <React.Fragment>
            <Header />

            <main className={styles.home}>
                <ul>
                    <li><Card /></li>
                    <li><Card /></li>
                    <li><Card /></li>
                    <li><Card /></li>
                </ul>
            </main>
        </React.Fragment>
    );
}