import React from 'react';
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import { HomePage } from "./pages/home/home";

const router = createBrowserRouter([
    {
        path: "/",
        element: <HomePage />,
    },
    {
        path: "/register",
        element: <div>Register</div>
    },
    {
        path: "/login",
        element: <div>Login</div>
    },
    {
        path: "/movie/:id",
        element: <div>Movie details</div>
    }
]);

function App() {
  return (
      <RouterProvider router={router} />
  );
}

export default App;

/*
    TODO: add side menu component
    TODO: add content component
    TODO: arrange components on `HomePage`
    TODO: add movie card component
 */